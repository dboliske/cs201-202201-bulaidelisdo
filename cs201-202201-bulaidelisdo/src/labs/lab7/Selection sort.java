import java.util.*;

public class Selection sort {
  public static void main(String[] args) {
   double [] ls=selectSort(new double[]{3.142,2.718,1.414,1.732,1.202,1.618,0.577,1.304,2.685,1.282});
    //write the array
        for (int i=0;i<ls.length;i++){
    System.out.println(ls[i]);}
  }
public static double[] selectSort(double[] arr) {
     
 
        for (int i = 0; i < arr.length - 1; i++) {  //because of the last number is the biggest
            int minIndex = i;                       //when you start,you need i=0arr[i]
            for (int j = i + 1; j < arr.length; j++) {  //behind of i[i+1，arr.length-1]is the least
                minIndex = arr[j] < arr[minIndex] ? j : minIndex;//The index is updated when then minimum value is found,and not updated if not
            }
            double temp = arr[i];          //swap the value
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
            
        
        }
            return arr;
    }
}