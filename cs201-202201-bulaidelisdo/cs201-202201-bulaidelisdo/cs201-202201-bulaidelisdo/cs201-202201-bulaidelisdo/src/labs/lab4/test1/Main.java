package test1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        GeoLocation g1 = new GeoLocation();
        g1.setLat(-10);
        g1.setLng(20);
        GeoLocation g2 = new GeoLocation(-10, 20);

        GeoLocation g3 = new GeoLocation(-10, 10);

        System.out.println("g1:" + g1);
        System.out.println("g2:" + g2);
        System.out.println("g3:" + g3);

        System.out.println("g1 equals g2 : " + g1.equals(g2)); // true
        System.out.println("g1 equals g3 : " + g1.equals(g3)); // false

//        Scanner scanner = new Scanner(System.in);
//        double v = scanner.nextDouble();
//        scanner.close();
//        boolean b = g1.validLat(v);
//        System.out.println(b);

        String s1 = "123";
        String s2 = "123";
        String s3 = new String("123");
        System.out.println(s1 == s2); // true
        System.out.println(s2 == s3); // false



    }

}
