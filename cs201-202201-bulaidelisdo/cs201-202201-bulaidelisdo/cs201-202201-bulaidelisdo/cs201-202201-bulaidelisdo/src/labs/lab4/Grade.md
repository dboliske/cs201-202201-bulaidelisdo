# Lab 4

## Total

18/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   1/1
* Documentation         0/3

## Comments

1. No documentation in any of your classes
2. Default constructors are not setting initial values for your instance variables
3. Non-default constructors and mutators are not doing any validation
