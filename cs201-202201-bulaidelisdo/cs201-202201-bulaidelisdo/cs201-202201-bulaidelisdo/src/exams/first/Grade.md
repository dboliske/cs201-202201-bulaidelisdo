# Midterm Exam

## Total

71/100

## Break Down

1. Data Types:                  15/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               2/5
    - Results:                  5/5
2. Selection:                   18/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  3/5
3. Repetition:                  18/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               3/5
4. Arrays:                      13/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     0/5
    - Results:                  3/5
5. Objects:                     7/20
    - Variables:                3/5
    - Constructors:             1/5
    - Accessors and Mutators:   3/5
    - toString and equals:      0/5

## Comments

1. You needed to confirm that the input is an integer and the result needs to be converted to a character, not a String.
2. Same input issue as Question 1.
3. Same issue as Questions 1 and 2.
4. Has an infinite loop and doesn't work when multiple words repeat.
5. Class named incorrectly, constructors do not initialize instance variables or have correct format, `setAge` does not validate `age`, and no `equals` or `toString` methods included.
