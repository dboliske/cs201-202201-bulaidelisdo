package labs.lab3;

import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class lab3two {
	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);

		System.out.println("please input number");

		String str1 = "";

		while (true) {
			String str2 = input.nextLine();
			if ("Done".equals(str2)) {
				break;
			}
			str1 += str2 + " ";
		}

		String[] str3 = str1.split(" ");
		int[] str4 = new int[str3.length];
		for (int i = 0; i <= str3.length - 1; i++) {
			if (!str3[i].equals("")) {
				str4[i] = Integer.parseInt(str3[i]);
			}
		}
		System.out.println("please input file name");
		String filename = input.nextLine();
		File file = new File(filename);
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		for (int j = 0; j < str3.length; j++) {
			bw.write(String.valueOf(str4[j]));
			if (j != str3.length) {
				bw.newLine();
			}

		}

		bw.flush();

		bw.close();
		fw.close();
		input.close();

	}

}