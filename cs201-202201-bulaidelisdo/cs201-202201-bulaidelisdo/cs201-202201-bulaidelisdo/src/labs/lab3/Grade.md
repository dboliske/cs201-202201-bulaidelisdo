# Lab 3

## Total

18/20

## Break Down

* Exercise 1    6/6
* Exercise 2    6/6
* Exercise 3    6/6
* Documentation 0/2

## Comments

Code is mostly good, though you should be using try/catch rather than throwing the exceptions. Additionally, no documentation again.
