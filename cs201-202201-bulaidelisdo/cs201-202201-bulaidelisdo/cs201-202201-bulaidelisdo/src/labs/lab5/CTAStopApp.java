package org.work.station;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CTAStopApp {
    /*
    CTAStopApp
    This application class will display a menu of options that the user can select from to display stations that meet certain criteria. The UML diagram for the class is as shown below, but here is the general description for each method:
    main:
    Calls readFile to load the data
    Passes that data to menu
    readFile:
    Reads stations from the input file and stores the data in an instance of CTAStation[].
    menu:
    Displays the menu options, which should be the following:
        1. Display Station Names
        2. Display Stations with/without Wheelchair access
        3. Display Nearest Station
        4. Exit
    Performs the operation requested by the user
    Loops until 'Exit' is chosen
    displayStationNames:
    Iterates through CTAStation[] and prints the names of the stations.
    displayByWheelchair:


    * */
    public static void main(String[] args) {
        CTAStopApp ctaStopApp = new CTAStopApp();
        CTAStation[] stations = ctaStopApp.readFile("src/CTAStops.csv");
        String menuStrings = "1. Display Station Names\n2. Display Stations with/without Wheelchair access\n3. Display Nearest Station\n4. Exit";
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println(menuStrings);
            System.out.print("input your choose: ");
            String s = input.nextLine();
            if (s.equals("1")) {
                ctaStopApp.displayStationNames(stations);
            } else if (s.equals("2")) {
                ctaStopApp.displayWheelchair(input, stations);
            } else if (s.equals("3")) {
                ctaStopApp.displayNearest(input, stations);
            } else if (s.equals("4")) {
                break;
            }

        }
        input.close();

    }

    public CTAStation[] readFile(String filename) {
        List<String> list = new ArrayList<>();
        try {
            File file = new File(filename);
            FileInputStream fileInputStream = new FileInputStream(file); // 建立和文件的输入流，相当于和文件建立输送管道
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream); // 程序读取输入流，相当于开启阀门，让水流进来
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader); // 建立读缓存，相当于用器皿接收水
            String tmp; // 将每次读到的数据存到tmp中
            while ((tmp = bufferedReader.readLine()) != null) {
                if (!"".equals(tmp)) {
                    list.add(tmp);
                }
            }
            bufferedReader.close();
            inputStreamReader.close();
            fileInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int size = list.size();
        CTAStation[] ctaStations = new CTAStation[size - 1];
        for (int i = 1; i < size; i++) {
            // Harlem,41.88706,-87.80486,elevated,TRUE,TRUE
            String s = list.get(i);
            String[] split = s.split(",");
            ctaStations[i - 1] = new CTAStation(
                    split[0], // name
                    Double.parseDouble(split[1]), // lat
                    Double.parseDouble(split[2]), // lng
                    split[3], // location
                    "TRUE".equals(split[4]) ? true : false, // wheelchair
                    "TRUE".equals(split[5]) ? true : false // open
            );
        }
        return ctaStations;
    }

    public void menu(CTAStation[] stations) {

    }

    public void displayStationNames(CTAStation[] stations) {
        String res = "";
        for(int i = 0; i < stations.length; i++) {
            res += stations[i].getName() + "\n";
        }
        System.out.println(res);
    }

    /*
    Prompts the user for accessibility ('y' or 'n')
    Checks that the input is 'y' or 'n', continues to prompt the user for 'y' or 'n' until one has been entered
    If char entered is a valid choice:Determine which boolean to use ('y' -> true, 'n' -> false)
    Loop through the CTAStation[] to look at wheelchair and display CTAStations that meet the requirement
    Display a message if no stations are found // not found
    */
    public void displayWheelchair(Scanner input, CTAStation[] stations) {
        System.out.print("please input y/n choose if support wheelchair: ");
        String s = input.nextLine();
        String res = "";
        boolean conditional = s.equals("y") ? true : false;
        for (int i = 0 ; i < stations.length; i++) {
            if (stations[i].hasWheelchair() == conditional) {
                res += stations[i].getName() + "\n";
            }
        }
        System.out.println(res);
        System.out.println("--------------------------------------------------");
    }
    /*
    displayNearest:
    Prompts the user for a latitude and longitude
    Uses the values entered to iterate through the CTAStation[] to find the nearest station (using calcDistance) and displays it to the console */
    public void displayNearest(Scanner input, CTAStation[] stations) {
        System.out.print("please input lat: ");
        double lat = input.nextDouble();
        System.out.print("please input lng: ");
        double lng = input.nextDouble();
        double min = Double.MAX_VALUE;
        // maybe have not only the nearest station
        double[] distances = new double[stations.length];

        for (int i = 0 ; i < stations.length; i++) {
            distances[i] = stations[i].calcDistance(lat, lng);
            if (min > distances[i]) {
                min = distances[i];
            }
        }
        System.out.println("follow are the nearest stations: ");
        for (int i = 0 ; i < distances.length; i++) {
            if (min == distances[i]) {
                System.out.println(stations[i]);
            }
        }
        System.out.println("--------------------------------------------------");

    }

}
