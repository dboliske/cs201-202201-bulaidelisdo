package org.work.station;

public class GeoLocation {
    private double lat;
    private double lng;

    public GeoLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public GeoLocation() {
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "(" + this.lat + ", " + this.lng + ")";
    }

    public boolean validLat(double lat) {
        return lat >= -90.0 && lat <= 90.0;
    }

    public boolean validLnt(double lnt) {
        return lnt >= -180.0 && lnt <= 180.0;
    }

    public boolean equals(GeoLocation geoLocation) {
        return this.lat == geoLocation.getLat() &&
                this.lng == geoLocation.getLng();
    }

    public double calcDistance(GeoLocation g) {
        // pow(a, b); == a ^ 2
        double powLat = Math.pow(this.lat - g.getLat(), 2);
        double powLng = Math.pow(this.lng - g.getLng(), 2);
        return Math.sqrt(powLat + powLng);

    }

    public double calcDistance(double lat, double lng) {
        return  Math.sqrt(Math.pow(this.lng - lng, 2) + Math.pow(this.lat - lat, 2));
    }


}