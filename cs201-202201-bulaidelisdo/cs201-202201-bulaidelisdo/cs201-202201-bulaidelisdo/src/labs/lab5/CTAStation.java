package org.work.station;

import java.util.Objects;

public class CTAStation extends GeoLocation {

    private String name;
    private String location;
    private boolean wheelchair;
    private boolean open;

    public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
        super(lat, lng);
        this.name = name;
        this.location = location;
        this.wheelchair = wheelchair;
        this.open = open;
//        this.setLat(lat);
//        this.setLng(lng);
    }

    public CTAStation() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean hasWheelchair() {
        return wheelchair;
    }

    public void setWheelchair(boolean wheelchair) {
        this.wheelchair = wheelchair;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public String toString() {
        return this.name + "," + this.getLat() + "," + this.getLng() + "," + this.getLocation() + "," + this.hasWheelchair() + "," + this.isOpen();
    }

    public boolean equals(CTAStation ctaStation) {
        return this.toString().equals(ctaStation.toString());
    }
}
