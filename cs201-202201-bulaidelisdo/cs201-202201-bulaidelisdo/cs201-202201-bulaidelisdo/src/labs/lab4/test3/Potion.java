package test3;

public class Potion {
    private String name;
    private double strength;

    public Potion () {

    }

    public Potion(String name, double strength) {
        this.name = name;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public double getStrength() {
        return strength;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public String toString() {
        return "name:" + this.name + ";" + "strength:" + this.strength;
    }

    public boolean validStrength(double strength) {
        return strength >= 0.0 && strength <= 10.0;
    }

    public boolean equals(Potion p) {
        return p == null ? false : this.toString().equals(p.toString());
    }




}
