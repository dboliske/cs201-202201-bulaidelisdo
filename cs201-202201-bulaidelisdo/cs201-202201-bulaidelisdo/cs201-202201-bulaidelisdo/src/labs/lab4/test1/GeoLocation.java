package test1;

import java.util.Objects;

public class GeoLocation {
    private double lat;
    private double lng;

    public GeoLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public GeoLocation() {
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        String format = "(" + this.lat + ", " + this.lng + ")";

//        String format = String.format("(%f, %f)", this.lat, this.lng);

        return format;
    }

    public boolean validLat(double lat) {
        return lat >= -90.0 && lat <= 90.0;
    }

    public boolean validLnt(double lnt) {
        return lnt >= -180.0 && lnt <= 180.0;
    }

    public boolean equals(GeoLocation geoLocation) {
        return this.lat == geoLocation.getLat() &&
                this.lng == geoLocation.getLng();
    }

}
