package test2;

public class PhoneNumber {
    private String countryCode;
    private String areaCode;
    private String number;

    public PhoneNumber() {
    }

    public PhoneNumber(String cCode, String aCode, String number) {
        this.countryCode = cCode;
        this.areaCode = aCode;
        this.number = number;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public String getNumber() {
        return number;
    }


    public void setCountryCode(String cCode) {
        this.countryCode = cCode;
    }

    public void setAreaCode(String aCode) {
        this.areaCode = aCode;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return countryCode + areaCode + number;
    }

    // 基本类型 byte char boolean int float double long
    // 引用类型
    public boolean validAreaCode(String aCode) {
        return aCode.length() == 3;
    }
    public boolean validNumber(String number) {

        return number.length() == 7;
    }

    public boolean equals(PhoneNumber pn) {

        if (pn == null) {
            return false;
        }

        // method1
//        String countryCode1 = pn.getCountryCode();
//        String areaCode1 = pn.getAreaCode();
//        String number1 = pn.getNumber();
//
//        return this.countryCode.equals(countryCode1)
//                && this.areaCode.equals(areaCode1)
//                && this.number.equals(number1);
        // method2
        String s1 = this.toString();
        String s2 = pn.toString();
        return s1.equals(s2);
    }


}
