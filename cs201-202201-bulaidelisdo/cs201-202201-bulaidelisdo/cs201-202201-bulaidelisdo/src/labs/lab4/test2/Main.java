package test2;

import test1.GeoLocation;

public class Main {

    public static void main(String[] args) {

        PhoneNumber pn1 = new PhoneNumber();
        pn1.setCountryCode("1001");
        pn1.setAreaCode("002");
        pn1.setNumber("1234567");

        PhoneNumber pn2 = new PhoneNumber("1001", "002", "1234567");
        PhoneNumber pn3 = new PhoneNumber("1001", "003", "1234567");

        System.out.println(pn1.toString());
        System.out.println(pn2.toString());

        System.out.println(pn1.equals(pn2)); // true
        System.out.println(pn2.equals(pn3)); // false

        System.out.println(pn1.validAreaCode("123")); // true
        System.out.println(pn1.validAreaCode("1234")); // false

        System.out.println(pn1.validNumber("0000000")); // true
        System.out.println(pn1.validNumber("00000001")); // false


    }
}
