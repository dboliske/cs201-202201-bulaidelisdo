# Lab 2

## Total

18/20

## Break Down

* Exercise 1    6/6
* Exercise 2    6/6
* Exercise 3    6/6
* Documentation 0/2

## Comments

Code looks good, but no documentation and the formatting is very hard to read.
